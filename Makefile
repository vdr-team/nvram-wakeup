# Makefile for nvram-wakeup
#
# $Id: Makefile 910 2008-11-23 13:40:45Z tiber $

# we rely on sed and msgfmt here.


# where do you want the program to be installed?
prefix   = /usr/local
BINDIR   = ${prefix}/bin
SBINDIR  = ${prefix}/sbin
MANDIR   = ${prefix}/man
DOCDIR   = ${prefix}/share/doc/nvram-wakeup
GTTXTDIR = ${prefix}/share/locale/de/LC_MESSAGES

PROG      = nvram-wakeup
PROG_SH   = VDR/vdrshutdown
HELPER    = rtc time biosinfo guess cat_nvram
HELPER_SH = guess-helper
DOC       = README README.mb README.reboot HISTORY nvram-wakeup.conf

BINS   = nvram-wakeup rtc time biosinfo guess cat_nvram
OBJS   = nvram-wakeup.o nvram-wakeup-mb.o bios.o gmt-test.o byteops.o \
         nvramops.o guess.o biosinfo.o tools.o readconf.o cat_nvram.o \
         rtc.o time.o
SRCS   = $(OBJS:%.o=%.c)
HDRS   = nvram-wakeup.h
GTTXT  = guess-helper.mo
GTTXTSRC = $(GTTXT:%.mo=%.po)
MAN5   = nvram-wakeup.conf.5
MAN8   = nvram-wakeup.8 biosinfo.8 cat_nvram.8 guess-helper.8 guess.8 \
         rtc.8 set_timer.8 time.8
MAN5GZ = $(MAN5:%.5=%.5.gz)
MAN8GZ = $(MAN8:%.8=%.8.gz)
DEVS = /dev/nvram /dev/rtc /dev/mem
CLEANFILES = $(BINS) $(OBJS) $(MAN5GZ) $(MAN8GZ) $(GTTXT) cvs_revs.h

# For the dist target:
TMPDIR = /tmp
VERSION = $(shell sed -ne '/define VERSION/s/^.*"\(.*\)".*$$/\1/p' nvram-wakeup.h)

CC = gcc
DEFS = -D_GNU_SOURCE
CFLAGS = -O2 -Wall -Wstrict-prototypes -g -pedantic $(DEFS)
INSTALL = install
INSTALL_STRIP = $(INSTALL) -s

all: $(BINS) $(GTTXT)
	size $(BINS)

nvram-wakeup:   nvram-wakeup.o nvram-wakeup-mb.o gmt-test.o byteops.o nvramops.o bios.o tools.o readconf.o 

biosinfo:	bios.o biosinfo.o tools.o

guess:          guess.o byteops.o bios.o tools.o 

cat_nvram:      cat_nvram.o tools.o nvramops.o

rtc:            rtc.o

time:           time.o

$(OBJS): $(HDRS) cvs_revs.h

cvs_revs.h: $(HDRS) $(SRCS)
	@echo Preparing $@
	@echo 'static char* CVS_ALL[] = {' >cvs_revs.h
	@grep $^ -e '[^"]$$Id' |sed "s/^.*\(\$$Id.*\$$\)/\"\1\",/">>cvs_revs.h
	@echo 'NULL };' >>cvs_revs.h
	
man: $(MAN5GZ) $(MAN8GZ)

%.gz: 
	gzip -cf9 `basename $@ .gz` >$@

%.mo: $(GTTXTSRC)
	msgfmt `basename $@ .mo`.po -o $@

devices: $(DEVS)

/dev/nvram:
	mknod $@ c 10 144

/dev/rtc:
	mknod $@ c 10 135

/dev/mem:
	mknod $@ c 1  1

clean:
	@$(RM) $(CLEANFILES)

rebuild:  clean all man

install-common:  all man
	@$(INSTALL) -vd $(BINDIR)
	@$(INSTALL) -vd $(SBINDIR)
	@$(INSTALL) -vd $(MANDIR)/man5
	@$(INSTALL) -vd $(MANDIR)/man8
	@$(INSTALL) -vd $(DOCDIR)
	@$(INSTALL) -vd $(GTTXTDIR)
	@$(INSTALL) -vm 755 $(PROG_SH)    $(BINDIR)
	@$(INSTALL) -vm 755 $(HELPER_SH)  $(SBINDIR)
	@$(INSTALL) -vm 644 $(MAN5GZ)     $(MANDIR)/man5
	@$(INSTALL) -vm 644 $(MAN8GZ)     $(MANDIR)/man8
	@$(INSTALL) -vm 644 $(DOC)        $(DOCDIR)
	@$(INSTALL) -vm 755 $(GTTXT)      $(GTTXTDIR)

install:  install-common
	@$(INSTALL) -vm 755 $(PROG)    $(BINDIR)
	@$(INSTALL) -vm 755 $(HELPER)  $(SBINDIR)

install-strip:  install-common
	@$(INSTALL_STRIP) -vm 755 $(PROG)    $(BINDIR)
	@$(INSTALL_STRIP) -vm 755 $(HELPER)  $(SBINDIR)

uninstall:
	cd $(BINDIR); rm -f $(PROG) $(subst VDR/,,$(PROG_SH))
	cd $(SBINDIR); rm -f $(HELPER) $(HELPER_SH)
	cd $(MANDIR)/man5; rm -f $(MAN5GZ)
	cd $(MANDIR)/man8; rm -f $(MAN8GZ)
	cd $(DOCDIR); rm -f $(DOC)
	cd $(GTTXTDIR); rm -f $(GTTXT)

tags: $(SRCS) $(HDRS)
	ctags $(SRCS) $(HDRS)

dist: clean
	@-rm -rf $(TMPDIR)/nvram-wakup-$(VERSION)
	@mkdir $(TMPDIR)/nvram-wakup-$(VERSION)
	@cp -a * $(TMPDIR)/nvram-wakup-$(VERSION)
	@tar czf nvram-wakup-$(VERSION).tar.gz -C $(TMPDIR) --exclude .svn nvram-wakup-$(VERSION)
	@-rm -rf $(TMPDIR)/nvram-wakup-$(VERSION)
	@echo Distribution package created as nvram-wakup-$(VERSION).tar.gz
