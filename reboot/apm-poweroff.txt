The file apm-poweroff.bin makes a direct APM BIOS call to turn off 
the PC. Use something like

   image = /boot/apm-poweroff.bin
   label = PowerOff

in your lilo.conf

Contributed by Andreas Rosenberg <andreas_rosenberg@web.de>
