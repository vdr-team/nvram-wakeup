/*
 *   NVRAM WakeUp
 *   Copyright (C) 2001-2005, Sergei Haller.
 *
 *   $Id: nvram-wakeup.h 923 2009-05-17 18:17:11Z tiber $
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef _NVRAM_WAKEUP_H_
#define _NVRAM_WAKEUP_H_ 1

#define CVSREV_nvram_wakeup_h \
     "$Id: nvram-wakeup.h 923 2009-05-17 18:17:11Z tiber $"

#define PACKAGE          "nvram-wakeup"
#define VERSION          "1.1"

#define ON   1
#define OFF  0

#define NVRAM_DEV        "/dev/nvram"
#define RTC_DEV          "/dev/rtc"
#define MEM_DEV          "/dev/mem"

#define MAXNVRAMSIZE     0x100  /* possible addresses 0x00, .., 0xFF */

/*
 * WAKEUP_BEFORE   is the number of minutes the machine needs for booting.  I.e.
 *                 we will set the wake up time  WAKEUP_BEFORE menutes before we
 *                 need the machine to be up and running.
 * NEED_TO_SHTDWN  is the number of minutes the machine needs between calling
 *                 nvram-wakeup and poweroff.
 *                 I.e. we don't accept wake up times less than 
 *                  (NEED_TO_SHTDWN + WAKEUP_BEFORE) minutes
 *                 in the future
 *
 * If NEED_TO_SHTDWN is too small, it may happen that the wake up time is passed
 * while  shutdown process,  so the machine  will wake  up one month  or (on the
 * boards, which store the month number) one year later than expected.
 * 
 * I think, it is sufficient to set NEED_TO_SHTDWN to 5 minutes but your mileage
 * may vary.
 */

#define WAKEUP_BEFORE    5
#define NEED_TO_SHTDWN   5

/*  needed for quoting the biosinfo  */ 
#define QUOTE(x) ((x == NULL) ? "" : "\"")
#define VALUE(x) ((x == NULL) ? "NULL" : x)


/* reboot conditions...  */
/* look into the man pages for explanation */

#define ON_STAT   1     /* (1 << 0) */
#define ON_SEC    2     /* (1 << 1) */
#define ON_MIN    4     /* (1 << 2) */
#define ON_HOUR   8     /* (1 << 3) */
#define ON_DAY    16    /* (1 << 4) */
#define ON_MON    32    /* (1 << 5) */
#define ON_WDAYS  64    /* (1 << 6) */

#define ON_ANY_CHANGE (~0)
#define ALWAYS        ON_ANY_CHANGE 

/* access methods for upper nvram (bytes 0x80-0xFF)   */
/* look into the man pages for explanation            */
/* sorted in the order I knew the appropriate method. */

#define INTEL     1
#define DS1685    2
#define VT82Cxxx  3
#define VT8235_37 4

/* algorithms for checksum computation.               */
/* look into the man pages for more information       */
 
#define DELL      1  /* Dell */
#define FSC       2  /* Fujitsu-Siemens */

struct biosinfo {
     int need_reboot;

     int addr_chk_h;
     int addr_chk_l;
     int addr_chk_h2;
     int addr_chk_l2;
     int addr_stat;
     int addr_mon;
     int addr_day;
     int addr_wdays;
     int addr_hour;
     int addr_min;
     int addr_sec;

     int shift_stat;
     int shift_mon;
     int shift_day;
     int shift_wdays;
     int shift_hour;
     int shift_min;
     int shift_sec;

     int rtc_time;
     int rtc_day;
     int rtc_mon;
     int rtc_day_0_is_c0;     /* 0 is actually 0xC0 */
     int rtc_mon_0_is_c0;     /* 0 is actually 0xC0 */

     int reset_day;
     int reset_mon;

     int nr_stat;
     int nr_mon;
     int nr_day;
     int nr_hour;
     int nr_min;
     int nr_sec;
     int nr_rtc_day;
     int nr_rtc_mon;
     int nr_wdays;

     int bcd;
     int day_no_bcd;
     int day_hack;
     int upper_method;

     int chk_method;
};

/* nvram-wakeup.c */

/* nvram-wakeup-mb.c */

int set_biosinfo_defaults(struct biosinfo *b);

/* bios.c */

int __dmi_probe(void);

/* gmt-test.c */

int compare_ltm_rtc(void);

/* readconf.c */

int readconf(struct biosinfo *b, const char * filename);

/* tools.c */

void         nvprintf(int lvl, char *fmt, ...);

char *       strip(char * s);

void         xxd(unsigned char * bytes, int size, int loglevel);
void         cat(unsigned char * bytes, int size);

const char * get_progname(void);
void         set_progname(const char * str);

void          enable_syslog(void);
void         disable_syslog(void);
void          enable_debug (void);
void         disable_debug (void);

/* byteops.c */

unsigned char bin2bcd( const unsigned char c );
unsigned char bcd2bin( const unsigned char c );

extern unsigned char calculate_read( const unsigned char b,
                                     const unsigned char nr,
                                     const unsigned char sh);
extern unsigned char calculate_write( const unsigned char b,
                                      const unsigned char _new,
                                      const unsigned char nr,
                                      const unsigned char sh);

/* bios.c */

/*  Determine the vendor of the system's BIOS.
 *  @return BIOS vendor or NULL on error */
char const * bios_vendor(void);

/*  Determine the version of the system's BIOS.
 *  @return BIOS version or NULL on error */
char const * bios_version(void);

/*  Determine the release of the system's BIOS.
 *  @return BIOS release or NULL on error */
char const * bios_release(void);


/*  Determine the vendor of the mainboard.
 *  @return mainboard vendor or NULL on error */
char const * board_vendor(void);

/*  Determine the type of the mainboard.
 *  @return mainboard type or NULL on error */
char const * board_type(void);

/*  Determine the version of the mainboard.
 *  @return mainboard version or NULL on error */
char const * board_version(void);


/* nvram-wakeup-mb.c */

int get_bios_info_by_dmi(struct biosinfo *b);
int get_bios_info_by_iw(struct biosinfo *b, const char * iw_name);

/* nvramops.c */

extern int  fd_rtc, fd_nvram;

void open_nvram(int _directisa, int mode);
void close_nvram(int _directisa);
int  read_whole_nvram(int _directisa, int upper_method, unsigned char * bytes);
void write_back(int _directisa, int upper_method, int addr, unsigned char * bytes);

#endif
