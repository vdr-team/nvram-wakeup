nvram-wakeup for Debian
=======================

nvram access methods
--------------------

There are two ways the NVRAM can be accessed. To use direct I/O access you
will have to specify the --directisa option. Alternatively you can use the
nvram kernel module. If this kernel module is available, but not loaded,
you may need to perform these steps:

    - modprobe nvram
    - add nvram to /etc/modules
    - alternatively you can use modconf to reach the same result


VDR integration
---------------

The original nvram-wakeup package has been created for the c't VDR project,
where an additional package vdr-addon-nvram-wakeup installed the necessary
shutdown hook script for VDR. This hook script is now included in nvram-wakeup
itself.

If you intend to use nvram-wakeup with VDR, be aware, that some mainboards
need to reboot once, before the wakeup time is activated. This requires a
special shutdown procedure, that may use a modified kernel image or special
kernel parameters to force booting into runlevlel 0. See
/usr/share/doc/README.reboot.gz for more detailed instructions. You can
configure a script or command sequence that is called when a reboot is needed
in /etc/vdr/vdr-nvram-wakeup.conf. i.e.:

SPECIALSHUTDOWN="lilo -R PowerOff ; shutdown -r now"

 -- Tobias Grimm <etobi@debian.org>  Sun, 23 Nov 2008 13:40:08 +0100

